db.fruits.insertMany([
  {
    name : "Apple",
    supplier: "Red Farms Inc.",
    stocks : 20,
    price: 40,
    onSale : true,
  },

  {
    name : "Banana",
    supplier : "Yellow Farms",
    stocks : 15,
    price: 20,
    onSale : true,
  },

  {
    name : "Kiwi",
    supplier : "Green Farming and Canning",
    stocks : 25,
    price: 50,
    onSale : true,
  },

  {
    name : "Mango",
    supplier : "Yellow Farms",
    stocks : 10,
    price: 60,
    onSale : true,
  },
  {
    name : "Dragon Fruit",
    supplier: "Red Farms Inc.",
    stocks : 10,
    price: 60,
    onSale : true,
  },

]);

db.fruits.aggregate([
  {$match: {supplier:"Red Farms Inc."}},
  {$count: "price"}
])

db.fruits.aggregate([
  {$match: {supplier:"Green Farming and Canning"}},
  {$count: "price"}
])

db.fruits.aggregate([
  {$match: {onSale:true}},
  {$group: {_id:"avgPriceOnSale", avgPrice: {$avg:"$price"}}}
])

db.fruits.aggregate([
  {$match: {onSale:true}},
  {$group: {_id:"maxPriceOnSale", maxPrice: {$max:"$price"}}]}
])

db.fruits.aggregate([
  {$match: {onSale:true}},
  {$group: {_id:"minPriceOnSale", minPrice: {$min:"$price"}}]}
])

